import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import * as utc from 'dayjs/plugin/utc';
import * as timezone from 'dayjs/plugin/timezone'; // dependent on utc plugin
import * as dayjs from 'dayjs';

dayjs.extend(utc);
dayjs.extend(timezone);

@Injectable()
export class CronService {
  constructor(
    @InjectRepository(User) private UserRepository: Repository<User>,
  ) {}
  private readonly logger = new Logger(CronService.name);

  sendBirthdayCard = async (item) => {
    await fetch('https://email-service.digitalenvision.com.au/send-email', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      body: JSON.stringify({
        email: item?.email,
        message: `Hey, ${item?.name} it's your birthday`,
      }),
    })
      .then(async () => {
        await this.UserRepository.createQueryBuilder()
          .update()
          .set({
            birthdayCard: null,
          })
          .where('id = :id', { id: item.id })
          .execute();
        this.logger.log('Successfully sent birthday greetings.');
      })
      .catch(async () => {
        await this.UserRepository.createQueryBuilder()
          .update()
          .set({
            birthdayCard: 'pending',
          })
          .where('id = :id', { id: item.id })
          .execute();
      });
  };

  @Cron(CronExpression.EVERY_MINUTE)
  async updateStayByStatus() {
    const users = await this.UserRepository.find();
    users.map(async (item) => {
      const currentTime = dayjs().tz(item.location).format('H:mm');
      const currentDate = dayjs().tz(item.location).format('DD-MM');
      const birthdayDate = dayjs(item.birthday)
        .tz(item.location)
        .format('DD-MM');

      if (item.birthdayCard === 'pending') {
        this.sendBirthdayCard(item);
      }

      if (currentDate === birthdayDate) {
        if (currentTime === '09:00') {
          if (item?.birthdayCard === null) {
            this.sendBirthdayCard(item);
          }
        }
      }
    });
    this.logger.log('Check birthday...');
  }
}
