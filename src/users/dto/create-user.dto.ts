import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    description: 'ISO 8601 string date format',
  })
  @IsNotEmpty()
  birthday: string;

  @ApiProperty({
    description:
      'Timezone location, exp: "America/Chicago". to get timezone location, you can use this code in javascript `Intl.DateTimeFormat().resolvedOptions().timeZone`',
  })
  @IsNotEmpty()
  location: string;

  @ApiProperty({
    description: 'this field is flag to automaticly send birthday greetings',
  })
  birthdayCard?: string;
}
