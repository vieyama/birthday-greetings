import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    description: 'ISO 8601 string date format',
  })
  @IsNotEmpty()
  birthday: string;

  @ApiProperty({
    description:
      'Timezone location, exp: "America/Chicago". to get timezone location, you can use this code in javascript `Intl.DateTimeFormat().resolvedOptions().timeZone`',
  })
  @IsNotEmpty()
  location: string;

  @ApiProperty({
    description: 'this field is flag to automaticly send birthday greetings',
  })
  birthdayCard?: string;
}
