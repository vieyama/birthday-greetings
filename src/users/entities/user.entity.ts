import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  birthday: string;

  @Column()
  location: string;

  @Column({
    default: null,
    nullable: true,
  })
  birthdayCard!: string;
}
