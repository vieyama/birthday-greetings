import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { EntityManager, Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private UserRepository: Repository<User>,
    private readonly entityManager: EntityManager,
  ) {}
  async create(createUserDto: CreateUserDto) {
    console.log(createUserDto);
    const user = new User();
    user.email = createUserDto.email;
    user.name = createUserDto.name;
    user.birthday = createUserDto.birthday;
    user.location = createUserDto.location;
    const createdUser = await this.entityManager.save(user);
    return { data: createdUser, message: 'success' };
  }

  async findAll() {
    const users = await this.UserRepository.find();
    return users;
  }

  async findOne(id: number) {
    try {
      const user = await this.UserRepository.findOneBy({ id });
      if (!user) {
        throw new HttpException('User not found', HttpStatus.BAD_GATEWAY);
      }
      return user;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    if (!id) {
      throw new HttpException('id cannot be empty', HttpStatus.BAD_GATEWAY);
    }
    const user = await this.UserRepository.findOneBy({ id });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_GATEWAY);
    }
    try {
      const update = await this.UserRepository.save({ id, ...updateUserDto });
      return { data: update, message: 'success' };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }

  async remove(id: number) {
    if (!id) {
      throw new HttpException('id cannot be empty', HttpStatus.BAD_GATEWAY);
    }
    const user = await this.UserRepository.findOneBy({ id });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_GATEWAY);
    }
    try {
      const deletedUser = await this.UserRepository.delete({ id });
      return { data: deletedUser, message: 'user deleted' };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
}
